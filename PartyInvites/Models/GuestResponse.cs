﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PartyInvites.Models
{
    public class GuestResponse
    {
        [Required(ErrorMessage = "Необходимо указать имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Необходимо указать email")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Вы ввели некорректный email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Необходимо указать номер телефона")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Необходимо указать свое согласие на посещение мероприятия")]
        public bool? WillAttend { get; set; }
    }
}